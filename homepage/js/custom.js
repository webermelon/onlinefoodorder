/* [Table of content]
=================
[01] Sticky Navbar
[02] Back To Top 
[03] Header Menu Active
[04] Owl Carousel Index Page
[05] lazyload js
[06] wow js
[06] menu js
[07] clock js
====================
[ End table of content]
====================*/

/* Spinner Loader */
$(window).on('load', function(){
    $("#spinner").fadeOut("1500");
});
/* Spinner Loader over here */

/* [01] Sticky Navbar on scroll START here */

$(document).ready(function () {
    // Custom function which toggles between sticky class (is-sticky)
    var stickyToggle = function (sticky, stickyWrapper, scrollElement) {
        var stickyHeight = sticky.outerHeight();
        var stickyTop = stickyWrapper.offset().top;
        if (scrollElement.scrollTop() >= stickyTop) {
            stickyWrapper.height(stickyHeight);
            sticky.addClass("is-sticky");
        } else {
            sticky.removeClass("is-sticky");
            stickyWrapper.height('auto');
        }
    };

    // Find all data-toggle="sticky-onscroll" elements
    $('[data-toggle="sticky-onscroll"]').each(function () {
        var sticky = $(this);
        var stickyWrapper = $('<div>').addClass('sticky-wrapper'); // insert hidden element to maintain actual top offset on page
        sticky.before(stickyWrapper);
        sticky.addClass('sticky');

        // Scroll & resize events
        $(window).on('scroll.sticky-onscroll resize.sticky-onscroll', function () {
            stickyToggle(sticky, stickyWrapper, $(this));
        });

        // On page load
        stickyToggle(sticky, stickyWrapper, $(window));
    });

/* Sticky Navbar on scroll OVER here */

    $(".navbar-toggler").click(function () {
        $('html').toggleClass('show-menu');
    });

    /* [02] Back To Top Button START Here */

    // hide #back-top first
    $("#toTop").hide();

    // fade in #back-top
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 500) {
                $('#toTop').fadeIn();
            } else {
                $('#toTop').fadeOut();
            }
        });

        // scroll body to 0px on click
        $('#toTop').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 1000);
            return false;
        });
    });

    /* Back to Top Button OVER Here */


/* [03] Header Menu Active START Here */
    $(".navbar-nav li a").each(function () {
        var pathname1 = window.location.href.substr(window.location.href.lastIndexOf('https://theme.whiteorangesoftware.com/') + 1);
        var pathname = pathname1.replace("#/", "");
        // console.log(pathname1);
        if ($(this).attr('href') == pathname) {
            $(this).parent().addClass('current-menu-item');
        } else if (pathname1 == '') {
            $('.navbar-nav li').first().addClass('current-menu-item')
        }
    });

    $(".navbar-nav li ul.dropdown-menu li a").each(function () {
        var pathname1 = window.location.href.substr(window.location.href.lastIndexOf('https://theme.whiteorangesoftware.com/') + 1);
        // alert(pathname1);
        var pathname = pathname1.replace("#/", "");
        if ($(this).attr('href').indexOf(pathname1) > -1) {
            $(this).parent().addClass('current-menu-item');
            $(this).parent().parent().parent().addClass('current-menu-item');
        }
    });
/* Header Menu Active OVER Here */

/* [04] Owl Carousel Index Page Testimonials START Here */

  var sync1 = $("#sync1");
  var sync2 = $("#sync2");
  var slidesPerPage = 3; //globaly define number of elements per page
  var syncedSecondary = true;

  sync1.owlCarousel({
    items : 1,
    slideSpeed : 2000,
    nav: true,
    center: true,
    autoplay: true,
    dots: false,
    loop: true,
    responsiveRefreshRate : 200,
    navText: ['<i class="fas fa-caret-left"></i>', '<i class="fas fa-caret-right"></i>'],
  }).on('changed.owl.carousel', syncPosition);

  sync2
    .on('initialized.owl.carousel', function () {
      sync2.find(".owl-item").eq(0).addClass("current");
    })
    .owlCarousel({
    dots: false,
    nav: false,
    smartSpeed: 200,
    slideSpeed : 500,
    responsiveRefreshRate : 100,
    responsive: {
        0: {
            items: 1
        },
        768: {
            items: 2,
            margin: 20
        },
        1199: {
            items: 3,
            margin: 20
        }
    }
  }).on('changed.owl.carousel', syncPosition2);

  function syncPosition(el) {
    
    var count = el.item.count-1;
    var current = Math.round(el.item.index - (el.item.count/2) - .5);
    
    if(current < 0) {
      current = count;
    }
    if(current > count) {
      current = 0;
    }
    
    //end block

    sync2
      .find(".owl-item")
      .removeClass("current")
      .eq(current)
      .addClass("current");
    var onscreen = sync2.find('.owl-item.active').length - 1;
    var start = sync2.find('.owl-item.active').first().index();
    var end = sync2.find('.owl-item.active').last().index();
    
    if (current > end) {
      sync2.data('owl.carousel').to(current, 100, true);
    }
    if (current < start) {
      sync2.data('owl.carousel').to(current - onscreen, 100, true);
    }
  }
  
  function syncPosition2(el) {
    if(syncedSecondary) {
      var number = el.item.index;
      sync1.data('owl.carousel').to(number, 100, true);
    }
  }
  
  sync2.on("click", ".owl-item", function(e){
    e.preventDefault();
    var number = $(this).index();
    sync1.data('owl.carousel').to(number, 300, true);
  });

  /* menu Start Here */
  $('.portfolio-item').isotope(function(){
      itemSelector:'.item'
    });

  $('.portfolio-menu ul li').click(function(){
    $('.portfolio-menu ul li').removeClass('active');
    $(this).addClass('active');

    var selector = $(this).attr('data-filter');
      $('.portfolio-item').isotope({
        filter: selector
      })
      return false;
  });

  // filter items on button click
  /* menu OVER Here */

});

/* [04] Owl Carousel Index Page Main Slider START Here */

$('#prime-carousel').owlCarousel({
    loop: true,
    margin: 0,
    autoplay: true,
    nav: true,
    dots: true,
    dotsContainer: ".banner_dots",
    navText: ['<i class="fas fa-caret-left"></i>', '<i class="fas fa-caret-right"></i>'],
    responsive: {
        0: {
            items: 1
        }
    }
});

/* [04] Owl Carousel Index Page Main Slider START Here */

$('#chefs-carousel').owlCarousel({
    loop: false,
    margin: 0,
    autoplay: true,
    nav: false,
    dots: true,
    navText: ['<i class="fas fa-caret-left"></i>', '<i class="fas fa-caret-right"></i>'],
    responsive: {
        0: {
            items: 1
        },
        480: {
            items: 2,
            margin: 20
        },
        768: {
            items: 3,
            margin: 20
        },
        1199: {
            items: 4,
            margin: 20
        }
    }
});
/* Owl Carousel Index Page Main Slider OVER Here */

/* profile social icon Start Here */

$('.hamburger').on('click', function(){
  $(this).toggleClass('open');
});

$(".social-icon").click(function () {
    $(this).toggleClass("spin");
});

/* profile social icon OVER Here */

/* lazyload Start Here */
$(function() {  $("img.lazyload").lazyload();  });
/* lazyload OVER Here */

/* wow js Start Here */

