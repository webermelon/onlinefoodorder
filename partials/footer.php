<!-- Footer Start -->
<footer class="footer-bg">
    <div class="container first-container">
        <div class="main-footer">
            <div class="f-1">
                <h6>Restaurant_Name_Here</h6>
                <p>
                    Lorem ipsum dolor sit amet, consectetur <br />
                    adipiscing elit, sed do eiusmod tempor incididunt ut <br />
                    labore et dolore magna aliqua. Quis ipsum <br />
                    suspendisse ultrices gravida.
                </p>
                <ul>
                    <li>
                        <a href="javascript:void(0)"><i class="fab fa-facebook-f"></i></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><i class="fab fa-instagram"></i></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><i class="fab fa-twitter"></i></a>
                    </li>
                </ul>
            </div>
            <div class="f-2">
                <h6>Important Links</h6>
                <ul>
                    <li><a href="home.php">Home</a></li>
                    <li><a href="about.php">About</a></li>
                    <li><a href="reservation.php">Reservation</a></li>
                    <li><a href="contact.php">Contact</a></li>
                    <li><a href="signup">Login</a></li>
                </ul>
            </div>
            <div class="f-3">
                <h6>Contact Us</h6>
                <address><i class="fas fa-map-marker-alt"></i> 35 E 10th St, New York, NY 10006</address>
                <p>
                    <a href="mailto:barelin@mail.com"><i class="fas fa-envelope"></i> barelin@mail.com</a>
                </p>
                <p>
                    <a href="tel:+11234567890"><i class="fas fa-phone-alt"></i> +1 123 456 7890</a>
                </p>
            </div>
        </div>
    </div>
    <div class="container-fluid bg-black">
        <div class="container">
            <div class="copyright">
                <p><a href="javascript:void(0)"> Copyright © restaurant_name_here 2020</a></p>
                <div class="sub-copy">
                    <p>Developed By <a href="https://webermelon.com" target="_blank" style="color: #5ebc27 !important;">WEBERMELON</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer End -->