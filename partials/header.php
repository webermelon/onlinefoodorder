<!-- Header Start -->
<header>
    <div class="brl-head">
        <div class="container">
            <div class="d-flex align-items-center justify-content-between">
                <!-- Contact information details Start -->
                <div class="brl-head-contact brl-head-col">
                    <div class="brl-head-address">
                        <i class="fas fa-map-marker-alt"></i>
                        <p>35 E 10th St, New York, NY 10006</p>
                    </div>
                    <div class="brl-head-direct-contact">
                        <div class="mr-5">
                            <i class="fas fa-envelope"></i>
                            <a href="mailto:barelin@gmail.com">barelin@gmail.com</a>
                        </div>
                        <div>
                            <i class="fas fa-phone"></i>
                            <a href="tel:+11234567890">+1 123 456 7890</a>
                        </div>
                    </div>
                </div>
                <!-- Contact information details End -->
                <!-- Product logo Start -->
                <a class="brand brl-head-col" href="#">
                    <img class="lazyload img-fluid" src="homepage/images/logo.png" data-srcset="homepage/images/logo.png" alt="logo" />
                </a>
                <!-- Product logo End -->
                <!-- Social connection information icons Start -->
                <div class="brl-head-social brl-head-col">
                    <div class="brl-social-wrapper">
                        <a href="javascript:void(0)">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </div>
                    <div class="brl-social-wrapper">
                        <a href="javascript:void(0)">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </div>
                    <div class="brl-social-wrapper">
                        <a href="javascript:void(0)">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </div>
                    <div class="brl-social-wrapper">
                        <a href="javascript:void(0)">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </div>
                </div>
                <!-- Social connection information icons End -->
            </div>
        </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-light" data-toggle="sticky-onscroll">
        <div class="container">
            <!-- Product logo Start -->
            <a class="brand brand-menu brl-head-col" href="#">
                <img class="lazyload img-fluid" src="homepage/images/logo.png" data-srcset="homepage/images/logo.png" alt="logo" />
            </a>
            <!-- Product logo End -->
            <!-- Product logo Menu-Fixed Start -->
            <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <div class="hamburger">
                    <span class="hm hm-1"></span>
                    <span class="hm hm-2"></span>
                    <span class="hm hm-3"></span>
                </div>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                <a class="brand menu-fixed" href="#">
                    <img class="lazyload img-fluid" src="homepage/images/logo.png" alt="logo" />
                </a>
                <!-- Product logo Menu-Fixed End -->
                <ul class="navbar-nav m-auto">
                    <!-- <li class="nav-item active"> -->
                    <li class="nav-item">
                        <a class="nav-link" href="home.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="menu-order" target="_blank">Menu</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about.php">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="gallery.php">Gallery</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="reservation.php">Reservation</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contact.php">Contact</a>
                    </li>
                </ul>
                <a href="signup" class="btn btn-brl-head-book">Login / Signup</a>
                <!-- Social connection information icons Start -->
                <div class="brl-head-social brl-head-col brl-head-menu">
                    <div class="brl-social-wrapper">
                        <a href="javascript:void(0)">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </div>
                    <div class="brl-social-wrapper">
                        <a href="javascript:void(0)">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </div>
                    <div class="brl-social-wrapper">
                        <a href="javascript:void(0)">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </div>
                    <div class="brl-social-wrapper">
                        <a href="javascript:void(0)">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </div>
                </div>
                <!-- Social connection information icons End -->
                <!-- Contact information details Start -->
                <div class="brl-head-contact brl-head-contact-menu brl-head-col">
                    <div class="brl-head-address">
                        <i class="fas fa-map-marker-alt"></i>
                        <p>35 E 10th St, New York, NY 10006</p>
                    </div>
                    <div class="brl-head-direct-contact">
                        <div class="mr-5">
                            <i class="fas fa-envelope"></i>
                            <a href="mailto:barelin@gmail.com">barelin@gmail.com</a>
                        </div>
                        <div>
                            <i class="fas fa-phone"></i>
                            <a href="%2b11234567890.html">+1 123 456 7890</a>
                        </div>
                    </div>
                </div>
                <!-- Contact information details End -->
            </div>
        </div>
    </nav>
</header>
<!-- Header End -->