<!DOCTYPE html>
<html lang="en">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        <title>Restaurant_Name_Here | HOME</title>
        <!-- meta tag -->
        <meta charset="utf-8" />
        <meta name="description" content="Online Ordering" />
        <meta name="keywords" content="Online Ordering" />
        <meta name="author" content="Webermelon" />
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- favicon-icon -->
        <link rel="icon" href="homepage/images/favicon.jpg" type="homepage/image/favicon.jpg" />
        <!-- font-awsome css -->
        <link rel="stylesheet" type="text/css" href="homepage/css/font-awsome.css" />
        <!-- bootstrap css -->
        <link rel="stylesheet" type="text/css" href="homepage/css/bootstrap.min.css" />
        <!-- owl.carousel css -->
        <link rel="stylesheet" type="text/css" href="homepage/css/owl.carousel.min.css" />
        <link rel="stylesheet" type="text/css" href="homepage/css/owl.theme.default.min.css" />
        <!-- jquery.fancybox.min css -->
        <link rel="stylesheet" type="text/css" href="homepage/css/jquery.fancybox.min.css" />
        <!-- style css -->
        <link rel="stylesheet" type="text/css" href="homepage/css/style.css" />
        <!-- responsive css -->
        <link rel="stylesheet" type="text/css" href="homepage/css/responsive.css" />

        <style>
        .d-none{
            display: none;
        }
        .col-center{
            float: none;
            margin: 0 auto;
        }
        .text-bold{
            font-weight: bold;
        }
        </style>
    </head>
    <body>
        <!-- Spinner loader Start -->
        <div id="spinner"></div>
        <!-- Spinner loader End -->


        <!-- Header Start -->
        <?php 
            $myRoot = $_SERVER["DOCUMENT_ROOT"];
            include($myRoot . '/barelin/partials/header.php');
        ?>
        <!-- Header End -->


        <!-- Contact Banner Start -->
        <section class="p-0">
            <div class="banner-img">
                <img class="lazyload" src="homepage/images/contact/contact-banner.jpg"
                    data-srcset="homepage/images/contact/contact-banner.jpg" alt="contact-banner" />
                <div class="banner-info">
                    <h5>Contact Us</h5>
                </div>
            </div>
        </section>
        <!-- Contact Banner End -->


        <!-- Contact Start -->
        <section>
            <div class="container">
                <div class="row <?php isset($_GET['Message']) ? print ' ' : print 'd-none' ?>">
                    <div class="col-md-6 col-center">
                        <div class="alert alert-success text-center text-bold">
                            <?php
                                if (isset($_GET['Message'])) {
                                    print $_GET['Message'];
                                }
                            ?>
                        </div>
                    </div>
                </div>

                <!-- <div class="map">
                    <iframe src="https://snazzymaps.com/embed/237866" style="border: none;"></iframe>
                    <div class="contact-main">
                        <div class="contact">
                            <div class="contact-photo">
                                <img class="lazyload" src="homepage/images/contact/map-pin.svg"
                                    data-srcset="homepage/images/contact/map-pin.svg" alt="map-pin" />
                            </div>
                            <div class="contact-info">
                                <p>Address</p>
                                <address>35 E 10th St, New York, NY 10006</address>
                            </div>
                        </div>
                        <div class="contact">
                            <div class="contact-photo">
                                <img class="lazyload" src="homepage/images/contact/mail.svg" data-srcset="homepage/images/contact/mail.svg"
                                    alt="mail" />
                            </div>
                            <div class="contact-info">
                                <p>Email</p>
                                <a href="mailto:barelin@mail.com">barelin@mail.com</a>
                            </div>
                        </div>
                        <div class="contact">
                            <div class="contact-photo">
                                <img class="lazyload" src="homepage/images/contact/call.svg" data-srcset="homepage/images/contact/call.svg"
                                    alt="call" />
                            </div>
                            <div class="contact-info">
                                <p>Phone</p>
                                <a href="tel:+11234567890">+1 123 456 7890</a>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="contact-form">
                    <h2 class="brl-section-title text-center">Direct Message Us</h2>
                    <form action="mail/config/contact.php" method="post">
                        <div class="contact-main-form">
                            <div class="form-input">
                                <input type="text" name="name" placeholder="Your Name *" required />
                                <input type="tel" name="phone" placeholder="Your Phone *" required />
                                <input type="email" name="email" placeholder="Email address *" required />
                            </div>
                            <div class="form-mesage">
                                <textarea name="message" placeholder="Messages" required></textarea>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-brl-head-book blog-btn">Send Message</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- Contact End -->


        <!-- Footer Start -->
        <?php 
            $myRoot = $_SERVER["DOCUMENT_ROOT"];
            include($myRoot . '/barelin/partials/footer.php');
        ?>
        <!-- Footer End -->


        <!-- To Top Button Start -->
        <div class="container">
            <div class="toTop" id="toTop">
                <i class="fad fa-angle-up"></i>
            </div>
        </div>
        <!-- To Top Button End -->


        <!-- jquery js -->
        <script src="homepage/js/jquery.min.js"></script>
        <!-- bootstrap js -->
        <script src="homepage/js/bootstrap.bundle.min.js"></script>
        <!-- jquery.fancybox.min js -->
        <script src="homepage/js/jquery.fancybox.min.js"></script>
        <!-- lazyload js -->
        <script src="homepage/js/lazyload.js"></script>
        <!-- owl.carousel js -->
        <script src="homepage/js/owl.carousel.min.js"></script>
        <!-- isotope.pkgd.min.js -->
        <script src="homepage/js/isotope.pkgd.min.js"></script>
        <!-- custom js -->
        <script src="homepage/js/custom.js"></script>
    </body>
</html>
