<!DOCTYPE html>
<html lang="en">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        <title>Restaurant_Name_Here | HOME</title>
        <!-- meta tag -->
        <meta charset="utf-8" />
        <meta name="description" content="Online Ordering" />
        <meta name="keywords" content="Online Ordering" />
        <meta name="author" content="Webermelon" />
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- favicon-icon -->
        <link rel="icon" href="homepage/images/favicon.jpg" type="homepage/image/favicon.jpg" />
        <!-- font-awsome css -->
        <link rel="stylesheet" type="text/css" href="homepage/css/font-awsome.css" />
        <!-- bootstrap css -->
        <link rel="stylesheet" type="text/css" href="homepage/css/bootstrap.min.css" />
        <!-- owl.carousel css -->
        <link rel="stylesheet" type="text/css" href="homepage/css/owl.carousel.min.css" />
        <link rel="stylesheet" type="text/css" href="homepage/css/owl.theme.default.min.css" />
        <!-- jquery.fancybox.min css -->
        <link rel="stylesheet" type="text/css" href="homepage/css/jquery.fancybox.min.css" />
        <!-- style css -->
        <link rel="stylesheet" type="text/css" href="homepage/css/style.css" />
        <!-- responsive css -->
        <link rel="stylesheet" type="text/css" href="homepage/css/responsive.css" />
    </head>
    <body>
        <!-- Spinner loader Start -->
        <div id="spinner"></div>
        <!-- Spinner loader End -->


        <!-- Header Start -->
        <?php 
            $myRoot = $_SERVER["DOCUMENT_ROOT"];
            include($myRoot . '/barelin/partials/header.php');
        ?>
        <!-- Header End -->


        <!-- Carousel Section Start -->
        <section class="p-0">
            <div class="w-100">
                <div class="brl-slider-wrapper">
                    <div class="brl-slide-caption-wrapper">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-md-2">
                                    <div class="blr-pattern-img blr-pattern-left">
                                        <img class="lazyload" src="homepage/images/pattern-slider-01.png" data-srcset="homepage/images/pattern-slider-01.png" alt="pattern-slider-01" />
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="brl-slide-captions-group">
                                        <h1 class="brl-heading">Barelin Restuarant</h1>
                                        <label>Ready to be opened for testy food</label>
                                    </div>
                                    <div class="banner_dots"></div>
                                </div>
                                <div class="col-md-2">
                                    <div class="blr-pattern-img blr-pattern-right">
                                        <img class="lazyload" src="homepage/images/pattern-slider.png" data-srcset="homepage/images/pattern-slider.png" alt="pattern-slider" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="prime-carousel" class="owl-carousel">
                        <!--Recommenced 01 -->
                        <div class="item">
                            <div class="item-img-wrapper">
                                <img class="lazyload" src="homepage/images/slider-one.jpg" data-srcset="homepage/images/slider-one.jpg" alt="slider-one" />
                            </div>
                        </div>
                        <!--Recommenced 02 -->
                        <div class="item">
                            <div class="item-img-wrapper">
                                <img class="lazyload" src="homepage/images/slider-one.jpg" data-srcset="homepage/images/slider-one.jpg" alt="slider-one" />
                            </div>
                        </div>
                        <!--Recommenced 03 -->
                        <div class="item">
                            <div class="item-img-wrapper">
                                <img class="lazyload" src="homepage/images/slider-one.jpg" data-srcset="homepage/images/slider-one.jpg" alt="slider-one" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Carousel Section End -->


        <!-- About Section Start -->
        <section class="brl-about-section">
            <div class="container">
                <div class="row">
                    <!-- About Section Content -->
                    <div class="col-md-6">
                        <div class="brl-about-content">
                            <h2 class="brl-section-title">
                                About <br />
                                Barelin Restaurant
                            </h2>
                            <p class="brl-about-text">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas. accumsan
                                lacus vel facilisis.
                            </p>
                            <p class="brl-about-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>

                            <p class="brl-resturant-info">Tuesday - Sunday <span> &nbsp; 10:00AM - 10:00PM</span></p>
                            <p class="brl-resturant-info">
                                <a href="reservation.php">Book Now</a><span> &nbsp; <a href="tel:+11234567890">+1 123 456 7890</a></span>
                            </p>
                        </div>
                    </div>
                    <!-- About Section Image-->
                    <div class="col-md-6">
                        <div class="brl-about-picture text-right">
                            <img class="lazyload" src="homepage/images/food.jpg" data-srcset="homepage/images/food.jpg" alt="food" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- About Section End -->


        <!-- Service Section Start -->
        <section class="brl-services">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h2 class="brl-section-title">Restaurant Services</h2>
                    </div>
                </div>
                <!-- Busniess Meeting Service -->
                <div class="row">
                    <div class="col-md-4 text-center">
                        <div>
                            <img class="lazyload" src="homepage/images/service-business_meeting.png" data-srcset="homepage/images/service-business_meeting.png" alt="service-business_meeting" />
                        </div>
                        <div>
                            <p class="brl-service-title">Business Meeting</p>
                            <p class="brl-service-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                        </div>
                    </div>
                    <!-- Birthday Party Service -->
                    <div class="col-md-4 text-center">
                        <div class="meeting-margin">
                            <img class="lazyload" src="homepage/images/service-birthday_party.png" data-srcset="homepage/images/service-birthday_party.png" alt="service-birthday_party" />
                        </div>
                        <div>
                            <p class="brl-service-title">Birthday Party</p>
                            <p class="brl-service-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                        </div>
                    </div>
                    <!-- Wedding Party Service -->
                    <div class="col-md-4 text-center">
                        <div class="meeting-margin">
                            <img class="lazyload" src="homepage/images/service-wedding_party.png" data-srcset="homepage/images/service-wedding_party.png" alt="service-wedding_party" />
                        </div>
                        <div>
                            <p class="brl-service-title">Wedding Party</p>
                            <p class="brl-service-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Service Section End -->


        <!-- Book A Table Start -->
        <section class="book-table-bg">
            <div class="container">
                <div class="row no-gutters justify-content-center">
                    <div class="col-lg-6 col-md-6">
                        <div class="table-booking">
                            <h4>Book A Table Online</h4>
                            <p>
                                Use our online reservation form to book your <br />
                                table in a restaurant.
                            </p>
                            <a href="reservation.php" class="btn btn-black">Book Now</a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="table-booking-menu">
                            <h4>Opening Hours</h4>
                            <div class="time-menu">
                                <p>Saturday</p>
                                <p>Closed</p>
                            </div>
                            <div class="time-menu active">
                                <p>Sunday</p>
                                <p>10:00AM - 10:00PM</p>
                            </div>
                            <div class="time-menu">
                                <p>Monday</p>
                                <p>10:00AM - 10:00PM</p>
                            </div>
                            <div class="time-menu">
                                <p>Tuesday</p>
                                <p>10:00AM - 10:00PM</p>
                            </div>
                            <div class="time-menu">
                                <p>Wednesday</p>
                                <p>10:00AM - 10:00PM</p>
                            </div>
                            <div class="time-menu">
                                <p>Thursday</p>
                                <p>10:00AM - 08:00PM</p>
                            </div>
                            <div class="time-menu">
                                <p>Friday</p>
                                <p>10:00AM - 08:00PM</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Book A Table End -->


        <!-- Gallery Start -->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h2 class="brl-section-title">Photo Gallery</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <a data-fancybox="gallery" href="homepage/images/gallery/gallery-img-1.jpg">
                            <div class="gallery-img">
                                <img class="lazyload" src="homepage/images/gallery/gallery-img-1.jpg" data-srcset="homepage/images/gallery/gallery-img-1.jpg" alt="gallery-img-1" srcset="homepage/images/gallery/gallery-img-1.jpg">
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a data-fancybox="gallery" href="homepage/images/gallery/gallery-img-2.jpg">
                            <div class="gallery-img">
                                <img class="lazyload" src="homepage/images/gallery/gallery-img-2.jpg" data-srcset="homepage/images/gallery/gallery-img-2.jpg" alt="gallery-img-2" srcset="homepage/images/gallery/gallery-img-2.jpg">
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a data-fancybox="gallery" href="homepage/images/gallery/gallery-img-3.jpg">
                            <div class="gallery-img">
                                <img class="lazyload" src="homepage/images/gallery/gallery-img-3.jpg" data-srcset="homepage/images/gallery/gallery-img-3.jpg" alt="gallery-img-3" srcset="homepage/images/gallery/gallery-img-3.jpg">
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a data-fancybox="gallery" href="homepage/images/gallery/gallery-img-4.jpg">
                            <div class="gallery-img">
                                <img class="lazyload" src="homepage/images/gallery/gallery-img-4.jpg" data-srcset="homepage/images/gallery/gallery-img-4.jpg" alt="gallery-img-4" srcset="homepage/images/gallery/gallery-img-4.jpg">
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a data-fancybox="gallery" href="homepage/images/gallery/gallery-img-5.jpg">
                            <div class="gallery-img">
                                <img class="lazyload" src="homepage/images/gallery/gallery-img-5.jpg" data-srcset="homepage/images/gallery/gallery-img-5.jpg" alt="gallery-img-5" srcset="homepage/images/gallery/gallery-img-5.jpg">
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a data-fancybox="gallery" href="homepage/images/gallery/gallery-img-6.jpg">
                            <div class="gallery-img">
                                <img class="lazyload" src="homepage/images/gallery/gallery-img-6.jpg" data-srcset="homepage/images/gallery/gallery-img-6.jpg" alt="gallery-img-6" srcset="homepage/images/gallery/gallery-img-6.jpg">
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a data-fancybox="gallery" href="homepage/images/gallery/gallery-img-7.jpg">
                            <div class="gallery-img">
                                <img class="lazyload" src="homepage/images/gallery/gallery-img-7.jpg" data-srcset="homepage/images/gallery/gallery-img-7.jpg" alt="gallery-img-7" srcset="homepage/images/gallery/gallery-img-7.jpg">
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a data-fancybox="gallery" href="homepage/images/gallery/gallery-img-8.jpg">
                            <div class="gallery-img">
                                <img class="lazyload" src="homepage/images/gallery/gallery-img-8.jpg" data-srcset="homepage/images/gallery/gallery-img-8.jpg" alt="gallery-img-8" srcset="homepage/images/gallery/gallery-img-8.jpg">
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <!-- Gallery End -->


        <!-- Footer Start -->
        <?php 
            $myRoot = $_SERVER["DOCUMENT_ROOT"];
            include($myRoot . '/barelin/partials/footer.php');
        ?>
        <!-- Footer End -->


        <!-- To Top Button Start -->
        <div class="container">
            <div class="toTop" id="toTop">
                <i class="fad fa-angle-up"></i>
            </div>
        </div>
        <!-- To Top Button End -->


        <!-- jquery js -->
        <script src="homepage/js/jquery.min.js"></script>
        <!-- bootstrap js -->
        <script src="homepage/js/bootstrap.bundle.min.js"></script>
        <!-- jquery.fancybox.min js -->
        <script src="homepage/js/jquery.fancybox.min.js"></script>
        <!-- lazyload js -->
        <script src="homepage/js/lazyload.js"></script>
        <!-- owl.carousel js -->
        <script src="homepage/js/owl.carousel.min.js"></script>
        <!-- isotope.pkgd.min.js -->
        <script src="homepage/js/isotope.pkgd.min.js"></script>
        <!-- custom js -->
        <script src="homepage/js/custom.js"></script>
    </body>
</html>
