<?php
/*POINTS PROGRAM*/
if (FunctionsV3::hasModuleAddon("pointsprogram")){
	unset($_SESSION['pts_redeem_amt']);
	unset($_SESSION['pts_redeem_points']);
}

$merchant_photo_bg=getOption($merchant_id,'merchant_photo_bg');
if ( !file_exists(FunctionsV3::uploadPath()."/$merchant_photo_bg")){
	$merchant_photo_bg='';
}

/*RENDER MENU HEADER FILE*/

/*GET MINIMUM ORDER*/

/*dump($distance);
dump($distance_type_raw);
dump($data['minimum_order']);*/

$min_fees=FunctionsV3::getMinOrderByTableRates($merchant_id,
   $distance,
   $distance_type_raw,
   $data['minimum_order']
);

//dump($min_fees);

$ratings=Yii::app()->functions->getRatings($merchant_id);
$merchant_info=array(
  'merchant_id'=>$merchant_id ,
  //'minimum_order'=>$data['minimum_order'],
  'minimum_order'=>$min_fees,
  'merchant_address'=>$data['merchant_address'],
  'restaurant_name'=>$data['restaurant_name'],
  'contact_phone'=>$data['contact_phone'],
  'restaurant_phone'=>$data['restaurant_phone'],
  'social_facebook_page'=>$social_facebook_page,
  'social_twitter_page'=>$social_twitter_page,
  'social_google_page'=>$social_google_page,
);
$this->renderPartial('/front/menu-header',$merchant_info);

/*ADD MERCHANT INFO AS JSON */
$cs = Yii::app()->getClientScript();
$cs->registerScript(
  'merchant_information',
  "var merchant_information =".json_encode($merchant_info)."",
  CClientScript::POS_HEAD
);


/*PROGRESS ORDER BAR*/
// $this->renderPartial('/front/order-progress-bar',array(
//    'step'=>3,
//    'show_bar'=>true
// ));

$now=date('Y-m-d');
$now_time='';

$todays_day = date("l");

$checkout=FunctionsV3::isMerchantcanCheckout($merchant_id);
$menu=Yii::app()->functions->getMerchantMenu($merchant_id , isset($_GET['sname'])?$_GET['sname']:'' , $todays_day );
//dump($menu);
//die();

//dump($checkout);

echo CHtml::hiddenField('is_merchant_open',isset($checkout['code'])?$checkout['code']:'' );

/*hidden TEXT*/
echo CHtml::hiddenField('restaurant_slug',$data['restaurant_slug']);
echo CHtml::hiddenField('merchant_id',$merchant_id);
echo CHtml::hiddenField('is_client_login',Yii::app()->functions->isClientLogin());

echo CHtml::hiddenField('website_disbaled_auto_cart',
Yii::app()->functions->getOptionAdmin('website_disbaled_auto_cart'));

$hide_foodprice=Yii::app()->functions->getOptionAdmin('website_hide_foodprice');
echo CHtml::hiddenField('hide_foodprice',$hide_foodprice);

echo CHtml::hiddenField('accept_booking_sameday',getOption($merchant_id
,'accept_booking_sameday'));

echo CHtml::hiddenField('customer_ask_address',getOptionA('customer_ask_address'));

echo CHtml::hiddenField('merchant_required_delivery_time',
  Yii::app()->functions->getOption("merchant_required_delivery_time",$merchant_id));

/** add minimum order for pickup status*/
$merchant_minimum_order_pickup=Yii::app()->functions->getOption('merchant_minimum_order_pickup',$merchant_id);
if (!empty($merchant_minimum_order_pickup)){
	  echo CHtml::hiddenField('merchant_minimum_order_pickup',$merchant_minimum_order_pickup);

	  echo CHtml::hiddenField('merchant_minimum_order_pickup_pretty',
         displayPrice(baseCurrency(),prettyFormat($merchant_minimum_order_pickup)));
}

$merchant_maximum_order_pickup=Yii::app()->functions->getOption('merchant_maximum_order_pickup',$merchant_id);
if (!empty($merchant_maximum_order_pickup)){
	  echo CHtml::hiddenField('merchant_maximum_order_pickup',$merchant_maximum_order_pickup);

	  echo CHtml::hiddenField('merchant_maximum_order_pickup_pretty',
         displayPrice(baseCurrency(),prettyFormat($merchant_maximum_order_pickup)));
}

/*add minimum and max for delivery*/
//$minimum_order=Yii::app()->functions->getOption('merchant_minimum_order',$merchant_id);
$minimum_order=$min_fees;
if (!empty($minimum_order)){
	echo CHtml::hiddenField('minimum_order',unPrettyPrice($minimum_order));
	echo CHtml::hiddenField('minimum_order_pretty',
	 displayPrice(baseCurrency(),prettyFormat($minimum_order))
	);
}
$merchant_maximum_order=Yii::app()->functions->getOption("merchant_maximum_order",$merchant_id);
 if (is_numeric($merchant_maximum_order)){
 	echo CHtml::hiddenField('merchant_maximum_order',unPrettyPrice($merchant_maximum_order));
    echo CHtml::hiddenField('merchant_maximum_order_pretty',baseCurrency().prettyFormat($merchant_maximum_order));
 }

$is_ok_delivered=1;
if (is_numeric($merchant_delivery_distance)){
	if ( $distance>$merchant_delivery_distance){
		$is_ok_delivered=2;
		/*check if distance type is feet and meters*/
		//if($distance_type=="ft" || $distance_type=="mm" || $distance_type=="mt"){
		if($distance_type=="ft" || $distance_type=="mm" || $distance_type=="mt" || $distance_type=="meter"){
			$is_ok_delivered=1;
		}
	}
}

echo CHtml::hiddenField('is_ok_delivered',$is_ok_delivered);
echo CHtml::hiddenField('merchant_delivery_miles',$merchant_delivery_distance);
echo CHtml::hiddenField('unit_distance',$distance_type);
echo CHtml::hiddenField('from_address', FunctionsV3::getSessionAddress() );

echo CHtml::hiddenField('merchant_close_store',getOption($merchant_id,'merchant_close_store'));
/*$close_msg=getOption($merchant_id,'merchant_close_msg');
if(empty($close_msg)){
	$close_msg=t("This restaurant is closed now. Please check the opening times.");
}*/
echo CHtml::hiddenField('merchant_close_msg',
isset($checkout['msg'])?$checkout['msg']:t("Sorry merchant is closed."));

echo CHtml::hiddenField('disabled_website_ordering',getOptionA('disabled_website_ordering'));
echo CHtml::hiddenField('web_session_id',session_id());

echo CHtml::hiddenField('merchant_map_latitude',$data['latitude']);
echo CHtml::hiddenField('merchant_map_longtitude',$data['lontitude']);
echo CHtml::hiddenField('restaurant_name',$data['restaurant_name']);


echo CHtml::hiddenField('current_page','menu');

if ($search_by_location){
	echo CHtml::hiddenField('search_by_location',$search_by_location);
}

echo CHtml::hiddenField('minimum_order_dinein',FunctionsV3::prettyPrice($minimum_order_dinein));
echo CHtml::hiddenField('maximum_order_dinein',FunctionsV3::prettyPrice($maximum_order_dinein));

/*add meta tag for image*/
Yii::app()->clientScript->registerMetaTag(
Yii::app()->getBaseUrl(true).FunctionsV3::getMerchantLogo($merchant_id)
,'og:image');

$remove_delivery_info=false;
if($data['service']==3 || $data['service']==6 || $data['service']==7 ){
	$remove_delivery_info=true;
}

/*CHECK IF MERCHANT SET TO PREVIEW*/
$is_preview=false;
if ($food_viewing_private==2){
	if (isset($_GET['preview'])){
		if($_GET['preview']=='true'){
			if(!isset($_GET['token'])){
				$_GET['token']='';
			}
			if (md5($data['password'])==$_GET['token']){
			   $is_preview=true;
			}
		}
	}
	if($is_preview==false){
		$menu='';
		$enabled_food_search_menu='';
	}
}
?>

<div class="sections section-menu section-grey2 menu-section-nikhil">
<div class="container">
  <div class="row">

     <div class="col-md-8 border menu-left-content">

        <div class="tabs-wrapper" id="menu-tab-wrapper">
				

		<ul id="tab">

		<!--MENU-->
	    <li class="active" id="menu_left_content" >
	        <div class="row">
			 <div class="col-md-4 col-xs-4 border category-list">
				<div class="theiaStickySidebar">
				 <?php
				 $this->renderPartial('/front/menu-category',array(
				  'merchant_id'=>$merchant_id,
				  'menu'=>$menu,
				  'show_image_category'=>getOption($merchant_id, 'merchant_show_category_image')
				 ));
				 ?>
				</div>
			 </div> <!--col-->
			 <div class="col-md-8 col-xs-8 border" id="menu-list-wrapper">

			 <?php if($enabled_food_search_menu==1):?>
			 <form method="GET" class="frm-search-food">

			 <?php
			 if($is_preview==true){
				 if(isset($_GET['preview'])){
				 	echo CHtml::hiddenField('preview','true');
				 }
				 if(isset($_GET['token'])){
				 	echo CHtml::hiddenField('token',$_GET['token']);
				 }
			 }
			 ?>

			 <div class="search-food-wrap">
			   <?php echo CHtml::textField('sname',
			   isset($_GET['sname'])?$_GET['sname']:''
			   ,array(
			     'placeholder'=>t("Search"),
			     'class'=>"form-control search_foodname required"
			   ))?>
			   <button type="submit"><i class="ion-ios-search"></i></button>
			 </div>
			 <?php if (isset($_GET['sname'])):?>
			     <a href="<?php echo Yii::app()->createUrl('store/menu-'.$data['restaurant_slug'])?>">
			     [<?php echo t("Clear")?>]
			     </a>
			     <div class="clear"></div>
			   <?php endif;?>
			 </form>
			 <?php endif;?>

			 <?php
			 $admin_activated_menu=getOptionA('admin_activated_menu');
			 $admin_menu_allowed_merchant=getOptionA('admin_menu_allowed_merchant');
			 if ($admin_menu_allowed_merchant==2){
			 	 $temp_activated_menu=getOption($merchant_id,'merchant_activated_menu');
			 	 if(!empty($temp_activated_menu)){
			 	 	 $admin_activated_menu=$temp_activated_menu;
			 	 }
			 }

			 $merchant_tax=getOption($merchant_id,'merchant_tax');
			 if($merchant_tax>0){
			    $merchant_tax=$merchant_tax/100;
			 }

			 switch ($admin_activated_menu)
			 {
			 	case 1:
			 		$this->renderPartial('/front/menu-merchant-2',array(
					  'merchant_id'=>$merchant_id,
					  'menu'=>$menu,
					  'disabled_addcart'=>$disabled_addcart
					));
			 		break;

			 	case 2:
			 		$this->renderPartial('/front/menu-merchant-3',array(
					  'merchant_id'=>$merchant_id,
					  'menu'=>$menu,
					  'disabled_addcart'=>$disabled_addcart
					));
			 		break;

			 	default:
				 	$this->renderPartial('/front/menu-merchant-1',array(
					  'merchant_id'=>$merchant_id,
					  'menu'=>$menu,
					  'disabled_addcart'=>$disabled_addcart,
					  'tc'=>$tc,
					  'merchant_apply_tax'=>getOption($merchant_id,'merchant_apply_tax'),
					  'merchant_tax'=>$merchant_tax>0?$merchant_tax:0,
					));
			    break;
			 }
			 ?>
			 </div> <!--col-->
			</div> <!--row-->
	    </li>
	    <!--END MENU-->




	    <!--BOOK A TABLE-->
	    <?php if ($booking_enabled):?>
	    <li>
	    <?php $this->renderPartial('/front/merchant-book-table',array(
	      'merchant_id'=>$merchant_id
	    )); ?>
	    </li>
	    <?php endif;?>
	    <!--END BOOK A TABLE-->

	    <!--PHOTOS-->
	    <?php if ($photo_enabled):?>
	    <li>
	    <?php
	    $gallery=Yii::app()->functions->getOption("merchant_gallery",$merchant_id);
        $gallery=!empty($gallery)?json_decode($gallery):false;
	    $this->renderPartial('/front/merchant-photos',array(
	      'merchant_id'=>$merchant_id,
	      'gallery'=>$gallery
	    )); ?>
	    </li>
	    <?php endif;?>
	    <!--END PHOTOS-->

	    <!--INFORMATION-->
	    <?php if ($theme_info_tab==""):?>
	    <li>
	        <div class="box-grey rounded " style="margin-top:0;">
	          <?php echo getOption($merchant_id,'merchant_information')?>
	        </div>
	    </li>
	    <?php endif;?>
	    <!--END INFORMATION-->

	    <!--PROMOS-->
	    <?php if ( $promo['enabled']==2 && $theme_promo_tab==""):?>
	    <li>
	    <?php
	     $this->renderPartial('/front/merchant-promo',array(
	      'merchant_id'=>$merchant_id,
	      'promo'=>$promo
	    )); ?>
	    </li>
	    <?php endif;?>
	    <!--END PROMOS-->


	   </ul>
	   </div>

     </div> <!-- menu-left-content-->


	 <div class="col-md-4" style="padding: 0px;">
		<?php if (getOptionA('disabled_website_ordering')!="yes"):?>
		<div id="menu-right-content" class="col-md-12 border menu-right-content <?php echo $disabled_addcart=="yes"?"hide":''?>" >

		<div class="theiaStickySidebar">
		<div class="box-grey rounded  relative">

			



			<!--CART-->
			<div class="inner line-top relative basket-section">

			<p style="font-size: 20px;font-weight: bold;"><?php echo t("Your Basket")?></p>

			<div class="item-order-wrap"></div>

			<!--VOUCHER STARTS HERE-->
			<?php //Widgets::applyVoucher($merchant_id);?>
			<!--VOUCHER STARTS HERE-->

			<!--MAX AND MIN ORDR-->
			<?php if ($minimum_order>0):?>
			<div class="delivery-min">
				<p class="small center"><?php echo Yii::t("default","Subtotal must exceed")?>
				<?php echo displayPrice(baseCurrency(),prettyFormat($minimum_order,$merchant_id))?>
			</div>
			<?php endif;?>

			<?php if ($merchant_minimum_order_pickup>0):?>
			<div class="pickup-min">
				<p class="small center"><?php echo Yii::t("default","Subtotal must exceed")?>
				<?php echo displayPrice(baseCurrency(),prettyFormat($merchant_minimum_order_pickup,$merchant_id))?>
			</div>
			<?php endif;?>

			<?php if($minimum_order_dinein>0):?>
			<div class="dinein-min">
				<p class="small center"><?php echo Yii::t("default","Subtotal must exceed")?>
				<?php echo FunctionsV3::prettyPrice($minimum_order_dinein)?>
			</div>
			<?php endif;?>

				<a href="javascript:;" class="clear-cart"><?php echo t("Clear")?></a>

			</div> <!--inner-->
			<!--END CART-->

			<!--DELIVERY OPTIONS-->
			<div class="inner line-top relative delivery-option center">

			<?php if ($remove_delivery_info==false):?>

			<?php else :?>
				<p class="bold"><?php echo t("Options")?></p>
			<?php endif;?>

			<?php echo CHtml::dropDownList('delivery_type',$now,
			(array)Yii::app()->functions->DeliveryOptions($merchant_id),array(
				'class'=>'grey-fields'
			))?>

			<?php
				if($website_use_date_picker==2){
					echo CHtml::dropDownList('delivery_date','',
					(array)FunctionsV3::getDateList($merchant_id)
					,array(
					'class'=>'grey-fields date_list'
					));
				} else {
					echo CHtml::hiddenField('delivery_date',$now);
					echo CHtml::textField('delivery_date1',
					FormatDateTime($now,false),array('class'=>"j_date grey-fields",'data-id'=>'delivery_date'));
				}
			?>

			<div class="delivery_asap_wrap">
				<?php $detect = new Mobile_Detect;?>
				<?php if ( $detect->isMobile() ) :?>
				<?php
				echo CHtml::dropDownList('delivery_time',$now_time,
				(array)FunctionsV3::timeList()
				,array(
				'class'=>"grey-fields"
				))
				?>
				<?php else :?>
				<?php
				$website_use_time_picker = getOptionA('website_use_time_picker');
				$delivery_time_list = FunctionsV3::getTimeList($merchant_id,$now);
				if($website_use_time_picker==3){
					echo CHtml::dropDownList('delivery_time','', (array)$delivery_time_list ,array(
						'class'=>'grey-fields time_list'
					));
				} else {
					$now_time=date("h:i A");
					echo CHtml::textField('delivery_time',$now_time,
					array('class'=>"timepick grey-fields",'placeholder'=>Yii::t("default","Delivery Time")));
				}
				?>
			<?php endif;?>

				<!-- <br>
				<b>Estimated Delivery Time: 1hour</b>
				<br>
				<br>
				<b>Estimated Pickup Time: 25min</b>
				<br> -->
				<br> 
				<?php if ( $checkout['is_pre_order']==2):?>
				<span class="delivery-asap">
				<?php echo CHtml::checkBox('delivery_asap',false,array('class'=>"icheck"))?>
					<span class="text-muted"><?php echo Yii::t("default","Delivery ASAP?")?></span>
				</span>
				<?php endif;?>
				<br>
				<br>

			</div><!-- delivery_asap_wrap-->

			<?php if ( $checkout['code']==1):?>
				<a href="javascript:;" class="orange-button medium checkout"><?php echo $checkout['button']?></a>
			<?php else :?>
				<?php if ( $checkout['holiday']==1):?>
					<?php echo CHtml::hiddenField('is_holiday',$checkout['msg'],array('class'=>'is_holiday'));?>
					<p class="text-danger"><?php echo $checkout['msg']?></p>
				<?php else :?>
					<p class="text-danger"><?php echo $checkout['msg']?></p>
					<p class="small">
					<?php echo Yii::app()->functions->translateDate(date('F d l')."@".timeFormat(date('c'),true));?></p>
				<?php endif;?>
			<?php endif;?>
			<br>
			<br>

			</div> <!--inner-->
			<!--END DELIVERY OPTIONS-->

		</div> <!-- box-grey-->
		</div> <!--end theiaStickySidebar-->

		</div> <!--menu-right-content-->
		<?php endif;?>



		<!-- ==================================================
		==================< Custom HTML Starts >===============
		=================================================== -->
		<div class="col-md-12">
			<div class="card">
				<div class="card-header text-center">
					<span class="text-center" style="font-size: 40px;">⚠️</span>
					<h4 class="card-title" style="margin-top: 10px;">FOOD ALLERGIES & DIETARY REQUIREMENTS</h4>
				</div>
				<div class="card-body">
					<p class="text-center">
						We are unable to guarantee that food prepared in our kitchen will be free from all traces of nuts, peanuts or tree nuts. If you have an allergy to these, or any other food products you must bring this to our attention. Ingredients used may not always be stated in the description (ie small extracts in sauces). There is also a high risk of accidential cross contamination. 
						<br>
						<br>
						For this reason, if you do suffer from allergies to any foods you undertake to dine here
						at your own risk.
						<br>
						<br>
						If you have any special dietary requirements or concerns about food ingredients, please talk to us – we are always willing to help and will do everything in our power to assist.
						<br>
						<br>
						<span class="text-bold"><b>We hope you enjoy your meal.</b></span>
					</p>
				</div>
			</div>
		</div>

		<div class="col-md-12" style="margin-top: 20px;">
			<div class="card">
				<div class="card-header text-center">
					<span class="text-center" style="font-size: 40px;">🌶️</span>
					<h4 class="card-title"  style="margin-top: 10px;">Our Spice Ratings</h4>
				</div>
				<div class="card-body">
					<p class="text-left">
						<div class="text-left">
							<span class="text-left"><i>🌶️</i> <b class="text-bold">Mild:</b> these foods are prepared to suit most diners</span>
						</div>
						<br>
						<div class="text-left">
							<span class="text-left"><i>🌶️🌶️</i> <b class="text-bold">Medium:</b> these dishes contain more spice but are unlikely to be ‘hot’</span>
						</div>
						<br>
						<div class="text-left">
							<span class="text-left"><i>🌶️🌶️🌶️</i> <b class="text-bold">Hot:</b> These dishes contain a mixture of spices which are likely to feel hot</span>
						</div>
						<br>
						<div class="text-left">
							<span class="text-left"><i>🌶️🌶️🌶️🌶️</i> <b class="text-bold">Very Hot:</b> For those with a real passion for something spicy </span>
						</div>
						<br>
						<div class="text-left">
							<span class="text-left"><i>🌶️🌶️🌶️🌶️🌶️</i> <b class="text-bold">Try Me If You Dare:</b> Our hottest dishes are for the experienced only.</span>
						</div>

						<br>
						<br>
						<span class="text-bold"><b>If we can make your favourite dish and alter the spice we will, just ask.</b></span>
					</p>
				</div>
			</div>
		</div>
		<!-- ==================================================
		==================< Custom HTML Ends >=================
		=================================================== -->
	 </div>
	 


  </div> <!--row-->
</div> <!--container-->
</div> <!--section-menu-->
