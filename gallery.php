<!DOCTYPE html>
<html lang="en">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        <title>Restaurant_Name_Here | Gallery</title>
        <!-- meta tag -->
        <meta charset="utf-8" />
        <meta name="description" content="Online Ordering" />
        <meta name="keywords" content="Online Ordering" />
        <meta name="author" content="Webermelon" />
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- favicon-icon -->
        <link rel="icon" href="homepage/images/favicon.jpg" type="homepage/image/favicon.jpg" />
        <!-- font-awsome css -->
        <link rel="stylesheet" type="text/css" href="homepage/css/font-awsome.css" />
        <!-- bootstrap css -->
        <link rel="stylesheet" type="text/css" href="homepage/css/bootstrap.min.css" />
        <!-- owl.carousel css -->
        <link rel="stylesheet" type="text/css" href="homepage/css/owl.carousel.min.css" />
        <link rel="stylesheet" type="text/css" href="homepage/css/owl.theme.default.min.css" />
        <!-- jquery.fancybox.min css -->
        <link rel="stylesheet" type="text/css" href="homepage/css/jquery.fancybox.min.css" />
        <!-- style css -->
        <link rel="stylesheet" type="text/css" href="homepage/css/style.css" />
        <!-- responsive css -->
        <link rel="stylesheet" type="text/css" href="homepage/css/responsive.css" />

        <style>
        .d-none{
            display: none;
        }
        .col-center{
            float: none;
            margin: 0 auto;
        }
        .text-bold{
            font-weight: bold;
        }
        </style>
    </head>
    <body>
        <!-- Spinner loader Start -->
        <div id="spinner"></div>
        <!-- Spinner loader End -->


        <!-- Header Start -->
        <?php 
            $myRoot = $_SERVER["DOCUMENT_ROOT"];
            include($myRoot . '/barelin/partials/header.php');
        ?>
        <!-- Header End -->


        <!-- Contact Banner Start -->
        <section class="p-0">
            <div class="banner-img">
                <img class="lazyload" src="homepage/images/contact/contact-banner.jpg"
                    data-srcset="homepage/images/contact/contact-banner.jpg" alt="contact-banner" />
                <div class="banner-info">
                    <h5>Photo Gallery</h5>
                </div>
            </div>
        </section>
        <!-- Contact Banner End -->


        <!-- Gallery Start -->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <a data-fancybox="gallery" href="homepage/images/gallery/gallery-img-1.jpg">
                            <div class="gallery-img">
                                <img class="lazyload" src="homepage/images/gallery/gallery-img-1.jpg" data-srcset="homepage/images/gallery/gallery-img-1.jpg" alt="gallery-img-1" srcset="homepage/images/gallery/gallery-img-1.jpg">
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a data-fancybox="gallery" href="homepage/images/gallery/gallery-img-2.jpg">
                            <div class="gallery-img">
                                <img class="lazyload" src="homepage/images/gallery/gallery-img-2.jpg" data-srcset="homepage/images/gallery/gallery-img-2.jpg" alt="gallery-img-2" srcset="homepage/images/gallery/gallery-img-2.jpg">
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a data-fancybox="gallery" href="homepage/images/gallery/gallery-img-3.jpg">
                            <div class="gallery-img">
                                <img class="lazyload" src="homepage/images/gallery/gallery-img-3.jpg" data-srcset="homepage/images/gallery/gallery-img-3.jpg" alt="gallery-img-3" srcset="homepage/images/gallery/gallery-img-3.jpg">
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a data-fancybox="gallery" href="homepage/images/gallery/gallery-img-4.jpg">
                            <div class="gallery-img">
                                <img class="lazyload" src="homepage/images/gallery/gallery-img-4.jpg" data-srcset="homepage/images/gallery/gallery-img-4.jpg" alt="gallery-img-4" srcset="homepage/images/gallery/gallery-img-4.jpg">
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a data-fancybox="gallery" href="homepage/images/gallery/gallery-img-5.jpg">
                            <div class="gallery-img">
                                <img class="lazyload" src="homepage/images/gallery/gallery-img-5.jpg" data-srcset="homepage/images/gallery/gallery-img-5.jpg" alt="gallery-img-5" srcset="homepage/images/gallery/gallery-img-5.jpg">
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a data-fancybox="gallery" href="homepage/images/gallery/gallery-img-6.jpg">
                            <div class="gallery-img">
                                <img class="lazyload" src="homepage/images/gallery/gallery-img-6.jpg" data-srcset="homepage/images/gallery/gallery-img-6.jpg" alt="gallery-img-6" srcset="homepage/images/gallery/gallery-img-6.jpg">
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a data-fancybox="gallery" href="homepage/images/gallery/gallery-img-7.jpg">
                            <div class="gallery-img">
                                <img class="lazyload" src="homepage/images/gallery/gallery-img-7.jpg" data-srcset="homepage/images/gallery/gallery-img-7.jpg" alt="gallery-img-7" srcset="homepage/images/gallery/gallery-img-7.jpg">
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a data-fancybox="gallery" href="homepage/images/gallery/gallery-img-8.jpg">
                            <div class="gallery-img">
                                <img class="lazyload" src="homepage/images/gallery/gallery-img-8.jpg" data-srcset="homepage/images/gallery/gallery-img-8.jpg" alt="gallery-img-8" srcset="homepage/images/gallery/gallery-img-8.jpg">
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <!-- Gallery End -->


        <!-- Footer Start -->
        <?php 
            $myRoot = $_SERVER["DOCUMENT_ROOT"];
            include($myRoot . '/barelin/partials/footer.php');
        ?>
        <!-- Footer End -->


        <!-- To Top Button Start -->
        <div class="container">
            <div class="toTop" id="toTop">
                <i class="fad fa-angle-up"></i>
            </div>
        </div>
        <!-- To Top Button End -->


        <!-- jquery js -->
        <script src="homepage/js/jquery.min.js"></script>
        <!-- bootstrap js -->
        <script src="homepage/js/bootstrap.bundle.min.js"></script>
        <!-- jquery.fancybox.min js -->
        <script src="homepage/js/jquery.fancybox.min.js"></script>
        <!-- lazyload js -->
        <script src="homepage/js/lazyload.js"></script>
        <!-- owl.carousel js -->
        <script src="homepage/js/owl.carousel.min.js"></script>
        <!-- isotope.pkgd.min.js -->
        <script src="homepage/js/isotope.pkgd.min.js"></script>
        <!-- custom js -->
        <script src="homepage/js/custom.js"></script>
    </body>
</html>
